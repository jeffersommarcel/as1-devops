window.onload=function(){
	list();
}

function create() {
  const user = {
    id: Math.random(),
    nome: document.getElementById('nome').value,
    sobrenome: document.getElementById('sobrenome').value,
    email: document.getElementById('email').value,
    dataNascimento: document.getElementById('dataNascimento').value,
  }

  const users = JSON.parse(localStorage.getItem('value'));
  users.push(user);
  localStorage.setItem('value', JSON.stringify(users));
  window.location.href = 'lista.html';
}

function deleteUser(id) {
  const users = JSON.parse(localStorage.getItem('value'));
  const update = users.filter(user => user.id != id);

  localStorage.setItem('value', JSON.stringify(update));
  location.reload()
}

function updatePage(id) {
  window.location.href = `atualizar.html?id=${id}`;
}

function list() {
  let dados = localStorage.getItem('value');
  if(dados === null) {
    return;
  }

  var tbody = document.getElementById("tbodyDados");
  dados = JSON.parse(dados);

  tbody.innerHTML = '';
  
  for(var i = 0; i < dados.length; i++){		
    const count = i + 1;	       
		tbody.innerHTML += '<tr>'+
								'<td>'+count+'</td>'+
								'<td>'+dados[i].nome+'</td>'+
								'<td>'+dados[i].sobrenome+'</td>'+
								'<td>'+dados[i].email+'</td>'+
								'<td>'+dados[i].dataNascimento+'</td>'+
								'<td><button class="btn btn-danger" onclick="deleteUser(\'' + dados[i].id + '\')">Excluir</button></td>'+
								'<td><button class="btn btn-primary" onclick="updatePage(\'' + dados[i].id + '\')">Alterar</button></td>'+
						   '</tr>';		
	}

}