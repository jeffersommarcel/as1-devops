window.onload=function(){
	getUser();
}

function getUser() {
  const urlSearchParams = new URLSearchParams(window.location.search);
  const params = Object.fromEntries(urlSearchParams.entries());


  const user = JSON.parse(localStorage.getItem('value')).find(user => user.id == params.id);

  document.getElementById('nome').value = user.nome;
  document.getElementById('sobrenome').value = user.sobrenome;
  document.getElementById('email').value = user.email;
  document.getElementById('dataNascimento').value = user.dataNascimento;
  document.getElementById('idUser').value = user.id;
}


function update() {
  const users = JSON.parse(localStorage.getItem('value'));
  const user = {
    id: document.getElementById('idUser').value,
    nome: document.getElementById('nome').value,
    sobrenome: document.getElementById('sobrenome').value,
    email: document.getElementById('email').value,
    dataNascimento: document.getElementById('dataNascimento').value,
  }

  users[users.findIndex(response => response.id == user.id)] = user;

  localStorage.setItem('value', JSON.stringify(users));
  window.location.href = 'lista.html';
}